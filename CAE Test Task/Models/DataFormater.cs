﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CAE_Test_Task.Models
{
    public class DataFormater
    {
        public DataFormater()
        {
           date = new List<string>();
           sum = new List<decimal>();
           count = new List<int>();
        }

        public void AddItem(string date, decimal sum, int count)
        {
            this.date.Add(date);
            this.sum.Add(sum);
            this.count.Add(count);
        }

        public List<string> date;
        public List<decimal> sum;
        public List<int> count;
    }
}
