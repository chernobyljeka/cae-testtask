﻿using System;
using System.Globalization;

namespace CAE_Test_Task.Models
{
    public static class FirstDayOfWeekUtility
    {

        private static DateTime GetDateFromWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        private static DateTime GetDateFromQuater(int year, int quarter)
        {
            DateTime firstDayOfQuarter = new DateTime(year, (quarter - 1) * 3 + 1, 1);
            return firstDayOfQuarter;
        }

        public static string GetWeekInterval(int year, int weekOfYear)
        {
            var date = GetDateFromWeek(year, weekOfYear);
            return String.Format("{0} - {1}", 
                date.ToString("dd/MM/yyyy"), 
                date.AddDays(7).AddHours(-1).ToString("dd/MM/yyyy"));
        }

        public static string GetQuaterInterval(int year, int quarter)
        {
            var date = GetDateFromQuater(year, quarter);
            return String.Format("{0} - {1}", 
                    date.ToString("dd/MM/yyyy"), 
                    date.AddMonths(3).
                    AddDays(-1).ToString("dd/MM/yyyy"));
        }
    }
}
