﻿using System;

namespace CAE_Test_Task.Models
{
    public class Sale
    {
        public int SaleID { get; set; }
        public DateTime SaleDate { get; set; }
        public decimal SaleSum { get; set; }
    }
}
