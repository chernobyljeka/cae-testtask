﻿using CAE_Test_Task.DataAccess;
using System;
using System.Globalization;
using System.Linq;

namespace CAE_Test_Task.Models
{
    public class DataWorker
    {
        private readonly AppDBContext _context;

        public enum Period { Quarter, Month, Week, Day }


        public DataWorker(AppDBContext context)
        {
            _context = context;
        }

        public DataFormater GetFormatedData(Period period, DateTime start, DateTime end)
        {
            DataFormater data = null;
            switch (period)
            {
                case Period.Day:
                    data = DayFilter(start, end);
                    break;
                case Period.Month:
                    data = MountFilter(start, end);
                    break;
                case Period.Week:
                    data = WeekFilter(start, end);
                    break;
                case Period.Quarter:
                    data = QuarterFilter(start, end);
                    break;
            }
            return data;
        }

        private DataFormater MountFilter(DateTime start, DateTime end)
        {
            var appDBContext = _context.Sales
            .Where(i => i.SaleDate >= start && i.SaleDate <= end)
            .GroupBy(i => new
            {
                month = i.SaleDate.Month,
                year = i.SaleDate.Year
            })
            .OrderBy(g => g.Key.year)
            .Select(group => new
            {
               date = String.Format("{0}/{1}", group.Key.month, group.Key.year),
               saleSum = group.Sum(sum => sum.SaleSum),
               saleCount = group.Count()
            });

            var data = new DataFormater();

            foreach (var item in appDBContext)
            {
                data.AddItem(item.date, item.saleSum, item.saleCount);
            }

            appDBContext = null;
            return data;
        }

        private DataFormater DayFilter(DateTime start, DateTime end)
        {
            var appDBContext = _context.Sales
                .Where(i => i.SaleDate >= start && i.SaleDate <= end)
                .OrderBy(i => i.SaleDate)
                .GroupBy(i => new {
                    day = i.SaleDate.Day,
                    month = i.SaleDate.Month,
                    year = i.SaleDate.Year
                })
                .Select(group => new
                {
                    date = String.Format("{0}.{1}.{2}", group.Key.day, group.Key.month, group.Key.year),
                    saleSum = group.Sum(sum => sum.SaleSum),
                    saleCount = group.Count()
                });

            var data = new DataFormater();
            foreach (var item in appDBContext)
            {
                data.AddItem(item.date, item.saleSum, item.saleCount);
            }

            appDBContext = null;
            return data;
        }

        private DataFormater WeekFilter(DateTime start, DateTime end)
        {
            var appDBContext = _context.Sales
            .Where(i => i.SaleDate >= start && i.SaleDate <= end)
            .GroupBy(i => new {
                week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(i.SaleDate,
                             CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday),
                year = i.SaleDate.Year})
            .OrderBy(i => i.Key.year)
            .Select(group => new
            {
                date = FirstDayOfWeekUtility.GetWeekInterval(group.Key.year, group.Key.week),
                saleSum = group.Sum(sum => sum.SaleSum),
                saleCount = group.Count()
            });

            var data = new DataFormater();

            foreach (var item in appDBContext)
            {
                data.AddItem(item.date, item.saleSum, item.saleCount);
            }

            appDBContext = null;
            return data;
        }

        private DataFormater QuarterFilter(DateTime start, DateTime end)
        {
            var appDBContext = _context.Sales
                .Where(i => i.SaleDate >= start && i.SaleDate <= end)
                .GroupBy(i => new {
                    quater = ((i.SaleDate.Month - 1) / 3 + 1),
                    year = i.SaleDate.Year
                })
                .OrderBy(g => g.Key.year)
                .Select(group => new
                {
                    date = FirstDayOfWeekUtility.GetQuaterInterval(group.Key.year, group.Key.quater),
                    saleSum = group.Sum(sum => sum.SaleSum),
                    saleCount = group.Count()
                });

            var data = new DataFormater();

            foreach (var item in appDBContext)
            {
                data.AddItem(item.date, item.saleSum, item.saleCount);
            }

            appDBContext = null;
            return data;
        }


    }
}
