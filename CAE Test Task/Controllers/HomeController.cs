﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CAE_Test_Task.DataAccess;
using CAE_Test_Task.Models;
using Microsoft.EntityFrameworkCore;

namespace CAE_Test_Task.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppDBContext _context; 

        public HomeController(AppDBContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("/Home/GetData/{period}&{start}&{end}")]
        public IActionResult GetData(DataWorker.Period period, DateTime start, DateTime end)
        {
            var dw = new DataWorker(_context);
            var data = dw.GetFormatedData(period, start, end);
            return Json(data);
        }

        [HttpGet("/Home/GetInitialData/")]
        public IActionResult GetInitialData()
        {
            var data = new List<DateTime>();

            data.Add(_context.Sales
                .Select(i => i.SaleDate).Min());
            data.Add(_context.Sales
                .Select(i => i.SaleDate).Max());

            return Json(data);
        }

        [HttpPost]
        public async Task<IActionResult> AddItem([Bind("SaleDate", "SaleSum")] Sale sale)
        {
            try
            {
                _context.Add(sale);
                await _context.SaveChangesAsync();
                return Json(_context.Sales.Last());
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("Add item error",
                    "Error: " + ex);
                return Content("error");
            }
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
