﻿(function () {
    'use strict';

    chartApp.controller('ChartCtrl', ['$scope', '$rootScope', '$filter', 'GetData', 'InitialData', 'moment',
        function ($scope, $rootScope, $filter, GetData, InitialData, moment) {
            $scope.chart;

            $scope.period = {
                model: 'Day',
                availableOptions: ['Quarter', 'Month', 'Week', 'Day']
            };

            $scope.start_date;
            $scope.end_date;

            $scope.defaultData = function () {
                InitialData.get({}).$promise.then(
                    function (dateObj) {
                        $scope.start_date = new Date(dateObj[0]);
                        $scope.end_date = new Date(dateObj[1]);
                        $scope.period.model = 'Day';
                        $scope.initData();
                    }
                );
            }

            $scope.defaultData();

            $scope.initData = function () {
                var start_date = $filter('date')($scope.start_date, "yyyy-MM-dd");
                var end_date = $filter('date')($scope.end_date, "yyyy-MM-dd");
                if (start_date && end_date) {
                    GetData.get({
                        period: $scope.period.model,
                        start: start_date,
                        end: end_date,
                    }, function (dateObj) {
                        $scope.labels = dateObj.date;
                        $scope.data = [
                            dateObj.sum,
                            dateObj.count
                        ];
                    });
                }
            };

            $scope.datasetOverride = [{
                type: "line",
                label: "Sum $/K",
                borderColor: 'rgba(200,180,50,1)',
                backgroundColor: 'rgba(200,180,50, 0.7)',
                fill: false,
                lineTension: 0,
                borderWidth: 3,
                pointBorderWidth: 3,
                pointBorderColor: 'rgba(200,180,50, 0.7)',
                pointBackgroundColor: 'rgba(255,255,255, 0.7)',
                pointRadius: 4,
                pointHoverBackgroundColor: 'rgba(255,255,255, 1)',
                pointHoverBorderColor: 'rgba(200,180,50, 1)',
                yAxisID: "y-sum",
            }, {
                label: "Sales",
                backgroundColor: 'rgba(175, 216, 248, 0.7)',
                borderColor: 'rgba(207, 150, 251, 0.8)',
                fill: false,
                cubicInterpolationMode: 'monotone',
                borderWidth: 3,
                yAxisID: "y-sale",
            }]

            $scope.options = {
                title: {
                    display: true,
                    text: 'CAE Test Task',
                },
                legend: {
                    display: true,
                    position: 'right',
                },
                tooltips: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        position: "left",
                        scaleLabel: {
                            display: true,
                            labelString: "Time",
                            fontColor: "black",
                        },
                        barPercentage: 1.0,
                        categoryPercentage: 1.0
                    }],
                    yAxes: [{
                        position: "left",
                        "id": "y-sum",
                        scaleLabel: {
                            display: true,
                            labelString: "Sum (in thousand)",
                            fontColor: "black",
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: 2500
                        }
                    }, {
                        position: "right",
                        "id": "y-sale",
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                elements: {
                    rectangle: {
                        borderWidth: 1,
                        borderColor: 'rgb(0, 0, 0)',
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                zoom: {
                    enabled: true,
                    drag: false,
                    mode: 'x',
                }
            };

            $scope.onClick = function (points, e) {
                var label = points[1]._model.label;
                if (label) {
                    var date_array = label.match(/([0-9]{2}.[0-9]{2}.[0-9]{4})/g);
                    switch ($scope.period.model) {
                        case 'Week':
                            $scope.$apply(function () {
                                $scope.start_date = moment(date_array[0], "DD-MM-YYYY").toDate();
                                $scope.end_date = moment(date_array[1], "DD-MM-YYYY").toDate();
                                $scope.period.model = 'Day';
                                $scope.initData();
                            });
                            break;
                        case 'Month':
                            $scope.$apply(function () {
                                var start = moment('01.' + label, "DD-MM-YYYY").toDate();
                                var end = moment('01.' + label, "DD-MM-YYYY").toDate();
                                end = moment(end).add(1, 'M');
                                end = moment(end).subtract(1, "days");

                                $scope.start_date = start;
                                $scope.end_date = moment(end, "DD-MM-YYYY").toDate();
                                $scope.period.model = 'Week';
                                $scope.initData();
                            });
                            break;
                        case 'Quarter':
                            $scope.$apply(function () {
                                $scope.start_date = moment(date_array[0], "DD-MM-YYYY").toDate();
                                $scope.end_date = moment(date_array[1], "DD-MM-YYYY").toDate();
                                $scope.period.model = 'Month';
                                $scope.initData();
                            });
                            break;
                    }
                }
            }

            $rootScope.$on("ApplyChangeChart", function (event, args) {
                $scope.defaultData();
            });

            $scope.change = function () {
                $scope.initData();
            };

            $scope.$on('chart-create', function (evt, chart) {
                $scope.chart = chart;
            });
        }]);

    chartApp.controller('AddItemCtrl', ['$scope', '$rootScope', '$filter', 'moment', 'AddData',
        function ($scope, $rootScope, $filter, moment, AddData) {

            $scope.newdate = new Date();
            $scope.sum = 0;

            $rootScope.changeChart = function () {
                $rootScope.$emit("ApplyChangeChart", { });
            };

            $scope.addData = function () {
                if ($scope.sum != 0) {
                    AddData.add({
                        SaleDate: $scope.newdate,
                        SaleSum: $scope.sum
                    }, function (dateObj) {
                        $scope.changeChart();
                    });
                }
                else
                {
                    alert("The SUM must be greater than 0.");
                }
            };

        }]);

})();
