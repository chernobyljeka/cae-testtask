﻿(function () {
    'use strict';

    chartApp.factory('GetData', function ($resource) {
        return $resource('/Home/GetData/:period&:start&:end', {}, {
            'get': { method: 'GET', isArray: false }
        });
    });
    chartApp.factory('InitialData', function ($resource) {
        return $resource('/Home/GetInitialData/', {}, {
            'get': { method: 'GET', isArray: true }
        });
    });

    chartApp.factory('AddData', function ($resource) {
        return $resource('/Home/AddItem/', { SaleDate: "@SaleDate", SaleSum: "@SaleSum" }, {
            'add': { method: 'POST'}
        });
    });
})();