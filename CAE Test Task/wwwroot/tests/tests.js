﻿describe('chartAppTest', function () {

    var mockResource, $httpBackend;
    beforeEach(angular.mock.module('chartApp'));

    beforeEach(function () {
        angular.mock.inject(function ($injector) {
            $httpBackend = $injector.get('$httpBackend');
            mockResource = $injector.get('InitialData');
        })
    });

    describe('getUser', function () {
        it('should call getUser with username', inject(function (InitialData) {
            $httpBackend.expectGET()
                .respond(["12.12.2012", "14.02.2015"]);

            var result = mockResource.get();

            $httpBackend.flush();

            expect(result[1]).toEqual("14.02.2015");
        }));

    });

});