﻿using Microsoft.EntityFrameworkCore;
using CAE_Test_Task.Models;
    
namespace CAE_Test_Task.DataAccess
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        public DbSet<Sale> Sales { get; set; }
       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sale>().ToTable("Sales");
        }
    }
}
