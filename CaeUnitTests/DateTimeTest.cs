using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CAE_Test_Task.Models;

namespace CaeUnitTests
{
    [TestClass]
    public class DateTimeTest
    {
        [TestMethod]
        public void TestDateTimeUtil()
        {
            //arrange 
            var year = 2017;
            var numberQuarter = 2;
            // act
            var res = FirstDayOfWeekUtility.GetQuaterInterval(year, numberQuarter);
            //Assert
            Assert.AreEqual("01.04.2017 - 30.06.2017", res);
        }

        [TestMethod]
        public void TestDateTimeUtilWeek()
        {
            //arrange 
            var year = 2017;
            var week = 1;
            // act
            var res = FirstDayOfWeekUtility.GetWeekInterval(year, week);
            //Assert
            Assert.AreEqual("02.01.2017 - 08.01.2017", res);
        }
    }
}
